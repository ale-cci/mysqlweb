import json
import os
import config as app_config
import qlite
import tunnel

#wsgian.utils.dot_env()

from wsgian import cfg
pard = cfg.default_configuration()
pard = app_config.global_parameters(pard)

# Server socket
port = pard['HTTP_PORT']
bind = '0.0.0.0:{port}'.format(port=port)

# Workers
#import multiprocessing
#workers = multiprocessing.cpu_count() * 2 + 1
workers = 2
keepalive = 60
worker_class = 'gthread'
threads = 4

if os.path.isdir("/dev/shm"):
	worker_tmp_dir = '/dev/shm'
else:
	worker_tmp_dir = None

# start
def on_starting(server):
	print('start')
	app_config.check_dir_tree(pard)
	app_config.check_db(pard)
	pard['sqlite.conn'] = qlite.DB(pard['SQLITE_DB'])
	tunnel.close_all_open_tunnel(pard)
	pard['sqlite.conn'].close()
	if pard.get('LOCAL_MODE'):
		print('*** LOCAL_MODE: ON ***')

# stop
def on_exit(server):
	pard['sqlite.conn'] = qlite.DB(pard['SQLITE_DB'])
	tunnel.close_all_open_tunnel(pard)
	pard['sqlite.conn'].close()
	print('exit')

# Logging
errorlog = "-"
loglevel = 'info'
accesslog = '-'
access_log_format = '%(h)s (%({x-forwarded-for}i)s) %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s %(L)s'

conf_data = {
	'bind': bind,
	'workers': workers,
	'keepalive': keepalive,
	'threads': threads,
	'worker_class': worker_class,
	'errorlog': errorlog,
	'loglevel': loglevel,
	'accesslog': accesslog,
	'access_log_format': access_log_format,
	'worker_tmp_dir': worker_tmp_dir,
}
#print(json.dumps(conf_data, indent=4))