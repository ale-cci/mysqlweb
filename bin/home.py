

def main(pard):
	pard['action'] = pard.get('action', 'start')
	pard['page_header'] = pard['TITLE']
	pard['javascript'] = ''
	pard['sidebar'] = ''
	pard['main_body'] = ''
	pard['msg'] = ''
	pard['error_msg'] = ''
	
	pard.setdefault('user_data', {})
	if pard['user_data']:
		pard['mw_user'] = pard['user_data']['user_id']
	
	pard['userbar'] = render_userbar(pard)
	
	# --------------------------- start ----------------------------
	if pard['action'] == 'start':
		if pard['user_data']:
			pard['redirect'] = '/prefs'
			return pard
		else:
			pard['main_body'] = render_home_page(pard) # 'Hello MySQLweb'
		
	elif pard['action'] == 'not_authorized':
		pard['main_body'] = '<h3>Not Authorized</h3>'

	else:
		pard['redirect'] = '/not_authorized'
		return pard
		#pard['msg'] = 'Function not available. ("%(action)s")' % pard
		#pard['main_body'] = '<input type="button" name="submit_form" value="Back" onclick="javascript:history.go(-1);">'
	
	pard['html'] = render_page(pard)
	return pard


def render_userbar(pard):
	pard.setdefault('user_data', {})
	el = []
	if pard['user_data']:
		el.append(pard['user_data']['email'])
		el.append(f'''<a href="{pard['LOGOUT_URL']}" title="Logout">Logout</a>''')
	else:
		el.append(f'''<a href="{pard['LOGIN_URL']}" title="Login">Login</a>''')
	userbar = '&nbsp;&nbsp;|&nbsp;&nbsp;'.join(el)	
	return userbar


def render_home_page(pard):
	html = '''
	<div style="text-align: left; padding-left: 10px; padding-bottom: 20px;">
	<h2>MySQLweb is a web application that run queries, create and edit data, view and export results.</h2>
	
	<img src="/static/icons/database_gear.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>DataBase Structure</b>&nbsp;&nbsp;<br>
	&nbsp;&nbsp;Database Structure Browser, provides instant access to database schema and objects.
	<br /><br />
	
	<img src="/static/icons/text_list_bullets.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>Connection Manager</b>&nbsp;&nbsp;<br>
	&nbsp;&nbsp;The Database Connections Panel enables developers to create, organize, and manage database connections
	<br /><br />
	
	<img src="/static/icons/clock.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>History Panel</b>&nbsp;&nbsp;<br>
	&nbsp;&nbsp;The History Panel provides complete session history of queries showing what queries were run and when. Developers can easily retrieve, review, re-run, append or modify previously executed SQL statement.
	<br /><br />

	<img src="/static/icons/lightning.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>Run current query</b>&nbsp;&nbsp;<br>
	<br />

	<img src="/static/icons/lightning_go.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>Run selected query</b>&nbsp;&nbsp;<br>
	<br />

	<img src="/static/icons/page_white_code.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>Comment selection</b>&nbsp;&nbsp;<br>
	<br />

	<img src="/static/icons/script_lightning.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>Exec Procedures</b>&nbsp;&nbsp;<br>
	&nbsp;&nbsp;Multiple queries can be executed simultaneously (in transaction - query have to be separated by semicolon)
	<br /><br />

	<img src="/static/img/save.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>Save SQL</b>&nbsp;&nbsp;<br>
	&nbsp;&nbsp;Developers can save and easily reuse <img src="/static/icons/folder_page.png" border="0"> common Queries
	<br /><br />

	<img src="/static/icons/page_white_put.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>Dump SQL</b>&nbsp;&nbsp;<br>
	&nbsp;&nbsp;Dumps results of a query to a file contains SQL insert statements.
				These stataments can be used for backup or transfer data to another SQL server.
	<br /><br />

	<img src="/static/icons/star.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>Query Autocomplete</b>&nbsp;&nbsp;<br>
	&nbsp;&nbsp;Press "esc" when you are typing queries and enjoy with <em>Autocoplete-query</em> feature!
	<br /><br />
	
	<img src="/static/icons/star.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>Query Wizard</b>&nbsp;&nbsp;<br>
	&nbsp;&nbsp;Type <code>wsel tablename [,tablename 2, ...]</code> and <em>run query</em>: MySQLweb prepares for you select statement to query the specified tables.<br />
	&nbsp;&nbsp;Type <code>wins tablename</code> and <em>run query</em>: MySQLweb prepares for you insert statement.
	<br /><br />
	
	<img src="/static/icons/star.png" border="0" style="float: left">
	&nbsp;&nbsp;<b>Silk Icons Search</b>&nbsp;&nbsp;<br>
	&nbsp;&nbsp;Type <code>silk icon-pattern</code> and <em>run query</em>: MySQLweb search <em>Silk</em> icon that matches the pattern specified.
	<br /><br />
	</div>

	<table width="100%%" class="header">
		<tr>
			<td class="center">GF&copy;</td>
		</tr>
	</table>
		''' % pard
	return html


def render_page(pard):
	html = """
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="author" content="gianfranco" />
	<title>%(TITLE)s</title>

%(CSS)s
<link rel="icon" href="/static/favicon.ico">
</head>
<body>

<!-- header -->
<div id="userbar">%(userbar)s</div>
<div id="header">%(page_header)s</div>

<!-- Sidebar -->
<div id="sidebar" style="display: none;">
%(sidebar)s
</div>

<!-- Main Body -->
<div id="main_body" align="center">
<form action="%(APPSERVER)slogin" method="post" name="theForm" id="theForm" autocomplete="off" spellcheck="false" enctype="multipart/form-data">
<!-- enctype="multipart/form-data" -->
<input type="hidden" id="action" name="action" value="%(action)s">
<input type="hidden" id="module" name="module" value="%(module)s">
%(msg)s
%(main_body)s
</form>
</div>

</body>
%(javascript)s
</html>
	""" % pard
	return html


