import csv
import io
import getpass
import json
import logging
import os
import pprint
import psutil
import signal
import subprocess
import sys
import time
import tools

import qlite
from urllib.parse import parse_qs
from html import escape

import paramiko
import forward

#import sshtunnel


class Error(Exception):
	"""Base class for other exceptions"""
	pass

class PasswordRequired(Error):
	"""Password Required"""
	pass

class ConnectionError(Error):
	"""Connection Error"""
	pass

class KeyNotFound(Error):
	"""Private or Agent Key not Found"""
	pass

class NotFound(Error):
	"""Raised when the record is not found"""
	pass

class DuplicateKey(Error):
	"""Key name already exist on preferences DB"""
	pass

class InvalidData(Error):
	"""Raised when input data is not valid"""
	pass

class InvalidStatus(Error):
	"""Raised when input data is not valid"""
	pass


db_structure = {
	'tunnel': '''
		CREATE TABLE tunnel (
			id                integer not null primary key autoincrement,
			user              text not null,
			name              text not null,
			ssh_server        text not null,
			ssh_user          text not null,
			ssh_pkey          text not null,
			ssh_pkey_password  text not null,
			ssh_allow_agent   text not null,
			db_host           text not null,
			local_bind_port   text not null,
			status            text not null,
			pid               integer not null,
			cmdline           text not null
			)
		''',
	'idx_tunnel_name': '''
		CREATE UNIQUE INDEX idx_tunnel_name 
			ON tunnel (name)
		''',
	}


def insert(pard, args):	
	args = check_args(pard, args)
	
	rec = get_by_name(pard, args)
	if rec:
		raise DuplicateKey('Duplicate Tunnel name for %(name)s' % args)
	ssh_connection(args, test=True)
	
	sql = '''
	INSERT INTO tunnel
		(user, name, ssh_server, ssh_user, ssh_pkey, ssh_pkey_password,
		 ssh_allow_agent, db_host, local_bind_port, status, pid, cmdline)
	VALUES
		(:user, :name, :ssh_server, :ssh_user, :ssh_pkey, :ssh_pkey_password,
		 :ssh_allow_agent, :db_host, :local_bind_port, '', 0, '')
		'''
	tunnel_id = pard['sqlite.conn'].execute(sql, args)
	pard['sqlite.conn'].commit()
	return tunnel_id


def update(pard, args):
	args = check_args(pard, args)
	rec = get_by_id(pard, args)
	if not rec:
		raise NotFound('Record not found')
	
	rec = get_by_name(pard, args)
	if rec and rec['id'] != args['id']:
		raise DuplicateKey('Duplicate Tunnel name for %(name)s' % args)
	
	if args['status'] == 'Connected':
		raise InvalidStatus('Can\'t update tunnel in status %(status)s' % args)
	
	ssh_connection(args, test=True)
	
	sql = '''
	update
		tunnel
	set
		name = :name,
		ssh_server = :ssh_server,
		ssh_user = :ssh_user,
		ssh_pkey = :ssh_pkey,
		ssh_pkey_password = :ssh_pkey_password,
		ssh_allow_agent = :ssh_allow_agent,
		db_host = :db_host,
		local_bind_port = :local_bind_port,
		status = :status,
		pid = :pid,
		cmdline = :cmdline
	where
		id = :id
		'''
	pard['sqlite.conn'].execute(sql, args)
	pard['sqlite.conn'].commit()
	return args


def check_args(pard, args):
	args.setdefault('user', 'gieffe')
	args.setdefault('name', '')
	args.setdefault('ssh_server', '')
	args.setdefault('ssh_user', '')
	args.setdefault('ssh_pkey', '')
	args.setdefault('ssh_pkey_password', '')
	args.setdefault('ssh_allow_agent', '1')
	args.setdefault('db_host', '')
	args.setdefault('local_bind_port', '')
	args.setdefault('status', '')
	args.setdefault('pid', 0)
	args.setdefault('cmdline', '')
	
	for k in ('name', 'ssh_server', 'ssh_user', 'db_host', 'local_bind_port'):
		if not args[k]:
			raise InvalidData('A value is required for the key `%s`' % k)
	
	try:
		args['local_bind_port'] = tools.to_int(
				args['local_bind_port'], 'Local bind port', lang='en')
	except Exception as err:
		raise InvalidData(str(err))
	
	if args['local_bind_port'] < 5000 or args['local_bind_port'] > 9999:
		raise InvalidData('Local Bind Port must be from 5000 to 9999')
		
	if args['ssh_allow_agent']:
		args['ssh_allow_agent'] = '1'
	else:
		args['ssh_allow_agent'] = ''
	return args

	
def set_status(pard, args):
	args.setdefault('id', 0)
	args.setdefault('status', '')
	sql = """
		update
			tunnel
		set
			status = :status
		where
			id = :id
		"""
	pard['sqlite.conn'].execute(sql, args)
	pard['sqlite.conn'].commit()
	return None


def set_pid(pard, args):
	args.setdefault('id', 0)
	args.setdefault('pid', 0)
	args.setdefault('cmdline', '')
	sql = """
		update
			tunnel
		set
			pid = :pid,
			cmdline = :cmdline
		where
			id = :id
		"""
	pard['sqlite.conn'].execute(sql, args)
	pard['sqlite.conn'].commit()
	return None


def get_by_id(pard, args):
	args.setdefault('id', 0)
	args['id'] = int(args['id'])
	sql = "select * from tunnel where id = :id"
	result = pard['sqlite.conn'].query(sql, args)
	return result[0] if result else {}
	

def get_by_name(pard, args):
	args.setdefault('name', '')
	sql = "select * from tunnel where name = :name"
	result = pard['sqlite.conn'].query(sql, args)
	return result[0] if result else {}


def get_list(pard, args=None):
	sql = "select * from tunnel order by name"
	result = pard['sqlite.conn'].query(sql)
	return result


def	export_to_csv(pard, args=None):
	sql = "select * from tunnel order by name"
	result = pard['sqlite.conn'].query(sql)
	fieldnames = (
		'user',
		'name',
		'ssh_server',
		'ssh_user',
		'ssh_allow_agent',
		'db_host',
		'local_bind_port')
	csvfile = io.StringIO()
	writer = csv.DictWriter(csvfile, fieldnames, extrasaction='ignore')
	writer.writeheader()
	for rec in result:
		writer.writerow(rec)
	csvfile.seek(0)
	buf = csvfile.read()
	csvfile.close()
	return buf


def import_from_csv(pard, buf):
	print(buf)
	if not isinstance(buf, str):
		raise InvalidData('Invalid input file')
	if not buf.startswith('user'):
		raise InvalidData('malformed header row')
	delimiter = buf[4]
	if delimiter not in (',', ';', '\t'):
		raise InvalidData('Invalid delimiter')
	
	csvfile = io.StringIO(buf)
	reader = csv.DictReader(csvfile, delimiter=delimiter)
	result = []
	for row in reader:
		rec = get_by_name(pard, {'name': row['name']})
		if rec:
			close_tunnel(pard, rec)
			time.sleep(1)
			rec.update(row)
			try:
				update(pard, rec)
				result.append('Entry `%(name)s` updated' % rec)
			except Error as err:
				result.append(f"Entry `{rec['name']}`not updated: {err}")
		else:
			try:
				insert(pard, row)
				result.append('Entry `%(name)s` inserted' % row)
			except Error as err:
				result.append(f"Entry `{row['name']}` not inserted: {err}")
	if not result:
		result.append('Nothing to load')
	return result


def delete(pard, args):
	args.setdefault('id', 0)
	sql = "delete from tunnel where id = :id and status = ''"
	pard['sqlite.conn'].execute(sql, args)
	pard['sqlite.conn'].commit()
	return None


def open_tunnel(pard, args):
	args.setdefault('id', 0)
	rec = get_by_id(pard, args)
	if not rec:
		raise NotFound('Tunnel not found')
	if rec['status'] != '':
		raise InvalidStatus('Can\'t open tunnel in status %(status)s' % rec)
	
	## Open ssh tunnel using subprocess module
	cmdline = [
		sys.executable,
		'-u',
		'-m',
		'tunnel',
		'_open_tunnel',
		'id=%(id)s' % rec
		]
	logfile = f"{pard['LOG_DIR']}/tunnel_{rec['name']}.log"
	
	p = subprocess.Popen(
		cmdline,
		stdout=open(logfile, 'w'),
		stderr=subprocess.STDOUT
		)
	
	## Save pid and command line arguments in sqlite tunnel table
	args['pid'] = p.pid
	args['cmdline'] = json.dumps(cmdline)
	set_pid(pard, args)
	
	## Let time to setup connection
	time.sleep(1)
	
	return p


def close_tunnel(pard, args):
	args.setdefault('id', 0)
	rec = get_by_id(pard, args)
	if not rec:
		raise NotFound('Tunnel not found')
	
	pid = int(rec['pid'])
	if not pid:
		return None
	
	killit = True
	
	## Check if there is a process with the pid specified
	try:
		os.kill(pid, 0)
	except OSError:
		killit = False
	
	## Retrive cmdline from process information and check if process
	## is a tunnel process
	if killit:
		cmdline = psutil.Process(pid).cmdline()
		if 'tunnel' not in cmdline:
			killit = False
	
	## Kill tunnel process
	if killit:
		os.kill(pid, signal.SIGINT)
	else:
		rec['status'] = ''
		set_status(pard, rec)
		rec['pid'] = 0
		rec['cmdline'] = ''
		set_pid(pard, rec)		
	
	return pid


def close_all_open_tunnel(pard, args=None):
	if not args:
		args = {}
	sql = '''
		select
			*
		from
			tunnel
		where
			status = 'Connected'
			and pid > 0
		'''
	result = pard['sqlite.conn'].query(sql, args)
	for rec in result:
		close_tunnel(pard, rec)
		print('Tunnel %(name)s closed' % rec)


def is_open(pard, args):
	args.setdefault('id', 0)
	args.setdefault('test_process', False)
	rec = get_by_id(pard, args)
	if not rec:
		raise NotFound('Tunnel not found')
	elif rec['status'] == 'Connected' and int(rec['pid']) > 0:
		if args['test_process']:
			pid = int(rec['pid'])
			try:
				os.kill(pid, 0)
				return True
			except OSError:
				return False
		else:
			return True
	else:
		return False


def _open_tunnel(pard, args):
	args.setdefault('id', 0)
	rec = get_by_id(pard, args)
	if not rec:
		raise NotFound('Tunnel not found')
	if rec['status'] != '':
		raise InvalidStatus('Can\'t open tunnel in status %(status)s' % rec)
	
	## Open Connection
	client = ssh_connection(rec, test=False)
	rhost, rport = get_host_port(rec['db_host'], 3306)
	
	## Set status to connected
	rec['status'] = 'Connected'
	set_status(pard, rec)
	
	## Set pid
	rec['pid'] = os.getpid()
	set_pid(pard, rec)
	
	## Register signal handler
	signal.signal(signal.SIGINT, lambda s, f: _close_tunnel(pard, rec))
	signal.signal(signal.SIGTERM, lambda s, f: _close_tunnel(pard, rec))
	
	## Forward tunnell
	try:
		local_port = int(rec['local_bind_port'])
		forward.forward_tunnel(
				local_port,
				rhost, rport,
				client.get_transport())
	except OSError as err:
		print(err)
		_close_tunnel(pard, rec)
	except Exception as err:
		print(err)
		_close_tunnel(pard, rec)
		


def _close_tunnel(pard, rec):
	rec['status'] = ''
	set_status(pard, rec)
	rec['pid'] = 0
	rec['cmdline'] = ''
	set_pid(pard, rec)
	print('Port forwarding stopped. Close Tunnel %(name)s' % rec)
	sys.exit(0)


def get_log(pard, args):
	args.setdefault('id', 0)
	rec = get_by_id(pard, args)
	logfile = f"{pard['LOG_DIR']}/tunnel_{rec['name']}.log"
	buf = open(logfile, 'r').read()
	return buf


def get_host_port(spec, default_port):
    "parse 'hostname:22' into a host and port, with the port optional"
    args = (spec.split(":", 1) + [default_port])[:2]
    args[1] = int(args[1])
    return args[0], args[1]


def ssh_connection(args, test=True):
	host, port = get_host_port(args['ssh_server'], 22)
	client = paramiko.SSHClient()
	client.load_system_host_keys()
	client.set_missing_host_key_policy(paramiko.WarningPolicy()) 
	if args['ssh_allow_agent']:
		agent_keys = paramiko.Agent().get_keys()
		if not agent_keys:
			raise KeyNotFound('Agent key not found')
		for key in agent_keys:
			try:
				client.connect(host, port, username=args['ssh_user'], pkey=key)
				if test:
					client.close()
				print(key.name, 'ok')
				break
			except Exception as err:
				print(key.name, err)
				continue
		else:
			raise ConnectionError('Unable to connect to %(ssh_server)s' % args)
	else:
		try:
			key = get_pkey(args['ssh_pkey'], args['ssh_pkey_password'])
		except Exception as err:
			raise ConnectionError(err, 'While loading pkey `%s`' % escape(args['ssh_pkey']))
		try:
			client.connect(host, port, username=args['ssh_user'], pkey=key)
			if test:
				client.close()
				print('ok')
		except Exception as err:
			print(err)
			raise ConnectionError('Unable to connect to %(ssh_server)s' % args)
	if test:
		return None	
	else:
		return client
	

def get_pkey(path='~/.ssh/id_dsa', password=None):
	keyfile = os.path.expanduser(path)
	key = paramiko.dsskey.DSSKey(filename=keyfile, password=password)
	return key


# -------------------------------------------------------------------------- #
# Query con apertura tunnel contestuale utilzzando la libreria sshtunnel
# -------------------------------------------------------------------------- #

# def ssh_query(pard):
# 	## Chiavi per server SSH
# 	pard.setdefault('SSH_SERVER', '127.0.0.1:22')
# 	pard.setdefault('SSH_USER', None)
# 	pard.setdefault('SSH_PKEY', None) # '~/.ssh/id_dsa'
# 	pard.setdefault('SSH_PKEY_PASSWORD', None)
# 	pard.setdefault('SSH_ALLOW_AGENT', False)
# 	
# 	## Chiavi per accedere a MySQL
# 	# MySQL Host: se il DB e' sulla stessa macchina che fa da
# 	#             SSH_SERVER bisogna mettere 127.0.0.1 o localhost
# 	#             la porta e' la porta su cui risponde MySQL su
# 	#             MySQL Host
# 	pard.setdefault('MySQL_HOST', '127.0.0.1:3306') 
# 	pard.setdefault('USER', '') # MySQL User
# 	pard.setdefault('DB', '') # MySQL DB
# 	pard.setdefault('PASSWORD', '') # MySQL Password
# 	
# 	pard.setdefault('verbose', False)
# 	
# 	ssh_server = get_host_port(pard['SSH_SERVER'], 22)
# 	remote_server = get_host_port(pard['MySQL_HOST'], 3306)
# 	
# 	if not pard['SSH_ALLOW_AGENT']:
# 		key = get_pkey(pard['SSH_PKEY'], pard['SSH_PKEY_PASSWORD'])
# 	else:
# 		key = None
# 	
# 	if pard['verbose']:
# 		logging.basicConfig()
# 		logging.getLogger("paramiko").setLevel(logging.INFO)
# 		sshtunnel.DEFAULT_LOGLEVEL = logging.INFO
# 			
# 	with sshtunnel.SSHTunnelForwarder(
# 			ssh_server,
# 			ssh_username=pard['SSH_USER'],
# 			allow_agent=pard['SSH_ALLOW_AGENT'],
# 			ssh_pkey=key,
# 			host_pkey_directories=[],
# 			remote_bind_address = remote_server) as tunnel:
# 		
# 		pard['HOST'] = '127.0.0.1:' + str(tunnel.local_bind_port)
# 		pard['sql']	= "select * from ced_risorse where stato <> 'deleted' limit 10"
# 		
# 		result = db.db_access.send_dict(pard)
# 	
# 	return result


def main():
	import config
	pard = config.global_parameters({})
	
	## Chiavi per server SSH
	pard['SSH_SERVER'] = '192.168.111.57'
	pard['SSH_USER'] = 'web'
	#pard['SSH_PKEY'] = '~/.ssh/id_dsa'
	#pard['SSH_PKEY_PASSWORD'] = getpass.getpass('SSH_PKEY_PASSWORD: ')
	pard['SSH_ALLOW_AGENT'] = True
	
	pard['MySQL_HOST'] = '127.0.0.1:6447'
	pard['USER'] = 'ceduser-mm'
	pard['DB'] = 'maxmara'
	pard['PASSWORD'] = 'kocodo25' #getpass.getpass()
	
	#pard['verbose'] = True
	
	result = ssh_query(pard)
	
	buf = tools.dict_to_table(pard, result['rows'], result['c_name'], 'ftext')
	print(buf)
# -------------------------------------------------------------------------- #

def test_database(pard, drop=False):
	db = qlite.DB(pard['SQLITE_DB'], db_structure)
	db.test_database(drop)


def test(pard):
	args = {}
	args['ssh_server'] = '192.168.111.57'
	args['ssh_user'] = 'web'
	args['ssh_allow_agent'] = False
	args['ssh_pkey'] = '~/.ssh/id_dsa'
	args['ssh_pkey_password'] = getpass.getpass('ssh_pkey_password: ')
	ssh_connection(args, test=True)


def _get_action(action):
	try:
		action = globals()[action]
	except KeyError:
		raise Exception('Funzione `%s` non presente nel modulo' % action)
	if not hasattr(action, '__call__'):
		raise Exception('Funzione `%s` non presente nel modulo' % action)
	return action


def _get_param(params):
	qs = params
	params = parse_qs(qs)
	params = {k: v[0] if len(v) == 1 else v for k,v in params.items()}
	return params


def run():
	import config
	pard = config.global_parameters({})
	action = _get_action(sys.argv[1])
	params = sys.argv[2] if len(sys.argv) > 2 else ''
	args = _get_param(params)
	print(action.__name__, params, sep=' -> ')
	pard['sqlite.conn'] = qlite.DB(pard['SQLITE_DB'])
	result = action(pard, args)
	pard['sqlite.conn'].close()
	pprint.pprint(result)


if __name__ == '__main__':
	run()