import json

#import db_access
import prefs
import db
import tools
import qq

def main(pard):
	if not prefs.load_user_auth(pard):
		pard['redirect'] = '/not_authorized'
		return pard
	pard['module'] = 'db_structure'
	pard['program'] = 'main'
	pard['action'] = pard.get('action', 'start')
	pard['menu'] = qq.render_menu(pard)
	pard['search_form'] = ''
	pard['result_msg'] = ''
	pard['separator'] = '' #'<hr width="100%" noshade>'
	pard['main_body'] = ''
	pard['javascript'] = '<script src="/static/javascripts/prototype.js" type="text/javascript"></script>\n'
	pard['javascript'] += ajax_js(pard)
	pard['result_msg'] = ''
	
	# --------------------------- start ----------------------------
	if pard['action'] in ('start', 'search'):
#		if pard['action'] == 'start':
# 			if 'table_search' in pard['sid_buffer']:
# 				pard['table_search'] = pard['sid_buffer']['table_search']
# 			if 'db_structure_table_name' in pard['sid_buffer']:
# 				pard['table_name'] = pard['sid_buffer']['db_structure_table_name']
# 		else:
# 			pard['sid_buffer']['table_search'] = pard['table_search']
		pard['action'] = 'search'
		pard['search_form'] = render_search_form(pard)
		
	# ------------ display_table_description -----------------------
	elif pard['action'] == 'display_table_description':
		pard['html'] = render_table_description(pard)
		return pard

	# ------------ show_create_table -----------------------
	elif pard['action'] == 'show_create_table':
		pard['html'] = render_show_create_table(pard)
		return pard

	
	else:
		pard['result_msg'] = tools.format_messaggio('Function not available. ("%(action)s")' % pard)
		pard['main_body'] = '<input type="button" name="submit_form" value="Back" onclick="javascript:history.go(-1);">'
	
	pard['html'] = render_page(pard)
	return pard


def render_page(pard):
	html = """
		<html>
		<head>
		%(CSS)s
		<title>%(TITLE)s</title>
		%(javascript)s
		<link rel="icon" href="/static/favicon.ico">
		</head>
		<body>
		<center>
		%(menu)s
		<form action="%(APPSERVER)sdb/%(ALIAS)s" method="post" name="%(module)s" enctype="multipart/form-data">
		<input type="hidden" name="module" value="%(module)s">
		<input type="hidden" name="action" value="%(action)s">
		%(search_form)s
		%(main_body)s
		</form>
		</center>
		</body>
		</html>
		""" % pard
	return html


def render_search_form(pard):
	pard['table_search'] = pard.get('table_search', '')
	pard['tables_list'] = render_tables_list(pard)
	pard['table_description'] = render_table_description(pard)
	html = """
	<table width="100%%">
	<tr>
		<td colspan="2">
			<img style="float: left; margin-top: 2px; margin-right: 0px;" src="/static/img/magnifier.png" border="0">
			<input id="table_search" type="text" name="table_search" value="%(table_search)s">
		</td>
	</tr>
	<tr>
		<td id="tables_list_area" width="20%%" style="vertical-align: top;">
			%(tables_list)s
		</td>
		<td id="table_description" width="80%%" style="vertical-align: top;">
			%(table_description)s
		</td>
	</tr>
	</table>
		<script type="text/javascript" language="javascript" charset="utf-8">
		document.getElementById('table_search').focus();
	</script>
	""" % pard
	return html


def render_tables_list(pard):
	tables_list = db.db_access.get_db_tables_list_like(pard)
	h = ['<ul id="table_list">']
	for table in tables_list:
		#table = table.encode('utf8')
		d = {}
		d['table'] = table
		hj = {'table_name': table}
		d['js'] = """display_table_description(%s);""" % json.dumps(hj)
		d['js_over'] = """onMouseOver="this.className='highlight'" onMouseOut="this.className='normal'" """
		d['js_query_data'] = "query_table_data('%s');" % table
		h.append("""
			<li %(js_over)s>
				<img src="/static/icons/database_lightning.png"
				     title="Query data"
				     onClick="%(js_query_data)s"
				 />
				 &nbsp;
				 <span onClick='%(js)s'>%(table)s</span>
			</li>
			""" % d)
	h.append('</ul>')
	return '\n'.join(h)


# def render_table_description(pard):
# 	pard['table_search'] = pard.get('table_search', '')
# 	pard['table_name'] = pard.get('table_name', pard['table_search'])
# 	if pard['table_name']:
# 		tables_list = db.db_access.get_db_tables_list(pard)
# 		if pard['table_name'] not in tables_list:
# 			return ''
# 	else:
# 		return ''
# 			
# 	describe = db.db_access.show_columns(pard)
# 	primary_key, index_list = db.db_access.show_index(pard)
# 	
# 	h = ["""
# 		<table width="100%%">
# 		<tr>
# 			<th>Field</th>
# 			<th>Type</th>
# 		</tr>
# 		"""]
# 	for rec in describe:
# 		if rec['Field'] in primary_key:
# 			rec['Field'] = '<b>%(Field)s</b>' % rec
# 			rec['Type'] = '<b>%(Type)s</b>' % rec
# 		h.append("""
# 			<tr>
# 				<td>%(Field)s</td>
# 				<td>%(Type)s <small><i>%(Extra)s</i></small></td>
# 			</tr>
# 			""" % rec)
# 	
# 	for rec in index_list:
# 		if rec['Non_unique']:
# 			rec['unique'] = ''
# 		else:
# 			rec['unique'] = 'Unique'
# 		rec['columns'] = ', '.join(rec['columns'])
# 		h.append("""
# 			<tr>
# 				<th colspan="2">%(unique)s Index "%(index_name)s"</th>
# 			</tr>
# 			<tr>
# 				<td colspan="2">%(columns)s</td>
# 			</tr>
# 			""" % rec)
# 	h.append('</table>')
# 	return '\n'.join(h)

def render_table_description(pard):
	pard['table_search'] = pard.get('table_search', '')
	pard['table_name'] = pard.get('table_name', pard['table_search'])
	#pard['sid_buffer']['db_structure_table_name'] = pard['table_name']
	if pard['table_name']:
		tables_list = db.db_access.get_db_tables_list(pard)
		if pard['table_name'] not in tables_list:
			return ''
	else:
		return ''
	describe = db.db_access.show_columns(pard)
	primary_key, index_list = db.db_access.show_index(pard)
	
	h = ["""
		<ul id="column_list">
			<li style="display: block; background-color: #DDDDDD; padding: 4px;">
				<img src="/static/img/table_gear.png" border="0" title="show create table" onClick='show_create_table({table_name: "%(table_name)s"});'>
				&nbsp;
				<b>%(table_name)s</b>
			</li>
		""" % pard]
	for rec in describe:
		if rec['Field'] in primary_key:
			rec['Field'] = '<b>%(Field)s</b>' % rec
			rec['Type'] = '<b>%(Type)s</b>' % rec
			rec['key_img'] = """style="list-style-image: url('/static/img/key.png');" """
		else:
			rec['key_img'] = ''
		rec['Type'] = rec['Type'].upper()
		h.append("""
			<li %(key_img)s>%(Field)s: %(Type)s <small><i>%(Extra)s</i></small></li>
			""" % rec)
	
	for rec in index_list:
		if rec['Non_unique']:
			rec['unique'] = ''
		else:
			rec['unique'] = 'Unique'
		h.append("""
			<li style="list-style-image: url('/static/img/folder_key.png');">%(unique)s Index "%(index_name)s"
			<ul>
			""" % rec)
		for key in rec['columns']:
			h.append("""<li style="list-style-image: url('/static/img/key.png');">%s</li>""" % key)
		h.append('</li></ul>')
	h.append('</ul>')
	return '\n'.join(h)


def render_show_create_table(pard):
	pard['show_create_table'] = db.db_access.show_create_table(pard)
	h = """
		<ul id="column_list">
			<li style="display: block; background-color: #DDDDDD; padding: 4px;">
				<img src="/static/img/table_gear.png" border="0" title="describe" onClick='display_table_description({table_name: "%(table_name)s"});'>
				&nbsp;
				<b>%(table_name)s</b>
			</li>
			<li style="list-style: none;">
			<pre>%(show_create_table)s</pre>
			</li>
		""" % pard
	return h


def ajax_js(pard):
	html = """
		<script type="text/javascript" language="javascript" charset="utf-8">

		function display_table_description(pard_js)
		{
			//@param pard_js.table_name
			pard_js.area = "table_description";
			params = "module=" + escape("%(module)s");
			params += "&action=display_table_description";
			params += "&area=table_description"
			params += "&table_name=" + escape(pard_js.table_name);
			req = new Ajax.Request
			(
				"%(APPSERVER)sdb/%(ALIAS)s",
				{
					method: 'post',
					parameters: params,
					onLoading: function()
					{
						$(pard_js.area).innerHTML = '<img src="/static/img/progress.gif">';
					},
					onComplete: function(resp)
					{
						$(pard_js.area).innerHTML = resp.responseText;
					}
				}
			)
		}

		function show_create_table(pard_js)
		{
			//@param pard_js.table_name
			pard_js.area = "table_description";
			params = "module=" + escape("%(module)s");
			params += "&action=show_create_table";
			params += "&area=table_description"
			params += "&table_name=" + escape(pard_js.table_name);
			_complete = 0;
			req = new Ajax.Request
			(
				"%(APPSERVER)sdb/%(ALIAS)s",
				{
					method: 'post',
					parameters: params,
					onLoading: function()
					{
						if (_complete == 0) {
							$(pard_js.area).innerHTML = '<img src="/static/img/progress.gif">';
						}
					},
					onComplete: function(resp)
					{
						_complete = 1;
						$(pard_js.area).innerHTML = resp.responseText;
					}
				}
			)
		}
		
		function query_table_data(table)
		{
			var query = 'select * from ' + escape(table);
			if (window.opener) {
				var opener_query_area = window.opener.document.getElementById('query_area');
				opener_query_area.value = query;
				window.opener.focus();
			}
		}

		</script>
		
		""" % pard
	return html