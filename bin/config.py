import tools
import pprint
import os
from db.dbapi import DatabaseError


def get_site_config():
	import yaml
	site_config_yaml = None
	if 'SITE_CONFIG_YAML' in os.environ:
		site_config_yaml = os.environ['SITE_CONFIG_YAML']
	elif os.path.isfile('site_config.yaml'):
		site_config_yaml = 'site_config.yaml'
	if site_config_yaml:
		buf = open(site_config_yaml, 'r').read()
		site_config = yaml.load(buf, Loader=yaml.CLoader)
	elif os.path.isfile('site_config.py'):
		import site_config
		site_config = site_config.config
	else:
		print('default_config')
		site_config = default_config()
	return site_config


def default_config():
	d = {
		'BASE_DIR': '..',
		'HTTP_PORT': 8088,
		'MAX_ROW': 100,
		'COOKIE_EXPIRES_DAYS': 10,
		'DEFAULT_USER': 'admin@mysqlweb.dev',
		'LOCAL_MODE': True,
		}
	return d


site_config = get_site_config()

def global_parameters(pard):
	#import site_config
	## Per sicurezza carico da site_config tutte e sole le chiavi che servono
	for k in ['HTTP_PORT', 'MAX_ROW', 'ENABLE_EVAL',
			  'BASE_DIR', 'DATA_DIR', 'COOKIE_EXPIRES_DAYS',
			  'SQLITE_DB', 'DEFAULT_USER', 'LOCAL_MODE']:
		pard[k] = site_config.get(k)
	
# 	if pard['HTTP_PORT'] != 80:
# 		pard['APPSERVER'] = 'http://' + pard['DNS'] + ':' + str(pard['HTTP_PORT']) + pard['APPSERVER_URI']
# 	else:
# 		pard['APPSERVER'] = 'http://' + pard['DNS'] + pard['APPSERVER_URI']
	
	pard['APPSERVER'] = '/'
	
	# --------------------------------------------------------------- #
	pard['BIN_DIR'] = pard['BASE_DIR'] + '/bin'
	pard['LOG_DIR'] = pard['BASE_DIR'] + '/log'
	if not pard['DATA_DIR']:
		pard['DATA_DIR'] = pard['BASE_DIR'] + '/data'
	pard['SID_DIR'] = pard['BASE_DIR'] + '/sid'
	pard['HTML_DIR'] = pard['BASE_DIR'] + '/html'
	pard['STATIC_DIR'] = pard['HTML_DIR']
	pard['IMG_DIR'] = pard['HTML_DIR'] + '/img'
	pard['ICON_DIR'] = pard['HTML_DIR'] + '/icons'
	pard['BACKUP_DIR'] = pard['BASE_DIR'] + '/bck'
	pard['COMMON_LOG_FILE'] = pard['LOG_DIR'] + '/common_log.txt'
	pard['PID_FILE'] = pard['LOG_DIR'] + '/mw.pid'
	pard['SID_LOG_FILE'] = pard['LOG_DIR'] + '/sid_log.txt'
	#pard['APPLICATION_LOG_FILE'] = pard['LOG_DIR'] + '/application_log.txt'
	pard['APPLICATION_LOG_FILE'] = 'stdout'
	pard['BATCH_LOG_FILE'] = pard['LOG_DIR'] + '/batch_log.txt'
	pard['SQL_LOG_FILE'] = pard['LOG_DIR'] + '/sql_log.txt'
	pard['SCGI_LOG_FILE'] = pard['LOG_DIR'] + '/scgi_log.txt'
	pard['LAST_CONN_FILE'] = pard['DATA_DIR'] + '/last_conn.txt'
	pard['PUBLISHING_DIR'] = pard['HTML_DIR']
	pard['DEBUG'] = 1
	pard['SID_TIMEOUT'] = 28800
	pard['DEBUG_SQL'] = 0
	pard['PROJECT'] = 'mw'
	pard['TITLE'] = 'MySQLWeb'
	pard['CSS'] = '<link rel="stylesheet" href="/static/sqlweb.css" type="text/css">'
	pard['DEFAULT_COLOR'] = '#2a68c8'
	# ------------------------------------------------------------- #	
	
	### wsgian ###
	if not pard['SQLITE_DB']:
		pard['SQLITE_DB'] = pard['DATA_DIR'] + '/mysqlweb.sqlite'
	if not pard['COOKIE_EXPIRES_DAYS']:
		pard['COOKIE_EXPIRES_DAYS'] = 10
	if not pard['LOCAL_MODE']:
		pard['LOCAL_MODE'] = False
	pard['APP_ERROR_LIST'] = (DatabaseError, )
	
	return pard


def check_dir_tree(pard):
	for key in ['LOG_DIR', 'DATA_DIR', 'SID_DIR', 'BACKUP_DIR']:
		tools.check_dir(pard[key])


def check_db(pard):
	import prefs
	import tunnel
	prefs.test_database(pard)
	tunnel.test_database(pard)


if __name__ == '__main__':
	result = get_site_config()
	pprint.pprint(result)