import pprint

import config
import qlite
import sys
import tunnel
import wsgian

## Config
from wsgian import cfg
pard = cfg.default_configuration()
pard = config.global_parameters(pard)

pard['CGI_ESCAPE'] = False
pard['AUTH_MODULE'] = 'users'
pard['LOGIN_MODULE'] = 'login'
pard['LOGIN_URL'] = '/login'
pard['LOGOUT_URL'] = '/logout'
pard['SIGNUP_URL'] = '/signup'

## URL
urls = [
	{'pattern': '/', 'module': 'home'},
	{'pattern': '/not_authorized', 'module': 'home', 'action': 'not_authorized'},
	{'pattern': '/prefs', 'module': 'prefs', 'login': True},
	{'pattern': '/tunnel', 'module': 'tunnel_prefs', 'login': 'admin'},
	{'pattern': '/users', 'module': 'users_prefs', 'login': 'admin'},
	{'pattern': '/qq/{alias}', 'module': 'qq', 'login': True},
	{'pattern': '/db/{alias}', 'module': 'db_structure', 'login': True},
	
	{'pattern': '/login', 'module': 'login'},
	{'pattern': '/logout', 'module': 'login', 'action': 'logout'},
	]

## The App
app = wsgian.App(urls, pard)

## Start and Stop functions
def on_start(pard):
	config.check_dir_tree(pard)
	config.check_db(pard)
	pard['sqlite.conn'] = qlite.DB(pard['SQLITE_DB'])
	tunnel.close_all_open_tunnel(pard)
	pard['sqlite.conn'].close()

def on_stop(pard):
	pard['sqlite.conn'] = qlite.DB(pard['SQLITE_DB'])
	tunnel.close_all_open_tunnel(pard)
	pard['sqlite.conn'].close()
	
# ----------------------------------------------------------------------------
if __name__ == '__main__':
	try:
		on_start(pard)
		wsgian.quickstart(app, pard['HTTP_ADDRESS'], pard['HTTP_PORT'])
	except KeyboardInterrupt:
		on_stop(pard)
		print('Esco')

