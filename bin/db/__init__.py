import importlib
import tunnel


def load_drivers(drivers):
	''' Load installed sql-drivers '''

	loaded_drivers = {}
	for driver in drivers:
		try:
			mod = importlib.import_module(f'.{driver}', 'db')
			loaded_drivers[driver] = mod.Driver
		except ImportError as e:
			print(f'[WARNING]: Unable to load driver {driver}: {e}')

	return loaded_drivers


DRIVERS = load_drivers([
	'mysql',
	'sqlite',
	'oracle',
	'mysql_click',
	'mssql',
])


class Wrapper(object):
	def __init__(self, fname):
		self.fname = fname
		#print '**Wrapper**', fname
	def __call__(self, pard, **args):
		pard.setdefault('DB', '')
		pard.setdefault('CONN_TYPE', 'Standard')
		pard.setdefault('TUNNEL_ID', 0)
		if pard['CONN_TYPE'] == 'SSH':
			if tunnel.is_open(pard, {'id': pard['TUNNEL_ID']}):
				pass
			else:
				tunnel.open_tunnel(pard, {'id': pard['TUNNEL_ID']})

		engine = pard.get('ENGINE')
		driver = DRIVERS[engine]
		return getattr(driver, self.fname)(pard, **args)


class DBaccess(object):
	def __init__(self):
		self.methods = {}
	def __getattr__(self, name):
		if name not in self.methods:
			self.methods[name] = Wrapper(name)
		return self.methods[name]

db_access = DBaccess()

def cfg(engine=''):
	import config
	import getpass
	pard = config.global_parameters({})
	if engine == 'sqlite':
		pard['HOST'] = ''
		pard['USER'] = ''
		pard['PASSWORD'] = '******'
		pard['DB'] = 'sqlite:/private/home/gm/altavoce/books.sqlite'
	elif engine == 'mysql':
		pard['HOST'] = 'localhost'
		pard['USER'] = 'devel'
		pard['PASSWORD'] = getpass.getpass()
		pard['DB'] = 'devel'
	elif engine == 'oracle':
		pard['HOST'] = 'test'
		pard['USER'] = 'tmprod'
		pard['PASSWORD'] = getpass.getpass()
		pard['DB'] = 'oracle:'
	else:
		pard['HOST'] = 'localhost'
		pard['USER'] = 'devel'
		pard['DB'] = 'devel'
		host = input('host [localhost]: ')
		user = input('user [devel]: ')
		db   = input('db [devel]:')
		pard['PASSWORD'] = getpass.getpass()
		if host:
			pard['HOST'] = host
		if user:
			pard['USER'] = user
		if db:
			pard['DB'] = db
	return pard

"""
To test db_access method from python interpreter:

>>> import db
>>> pard = db.cfg('mysql')
>>> db.db_access.method(pard)
>>> ...
"""
