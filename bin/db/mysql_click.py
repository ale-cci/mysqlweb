import MySQLdb

from .dbapi import DBApi, DatabaseError

MySQLError = MySQLdb.DatabaseError


class Driver(DBApi):
	
	@classmethod
	def connect(cls, pard):
		host = pard['HOST']
		user = pard['USER']
		passwd = pard['PASSWORD']
		db = pard['DB']
		pair = host.split(':')
		if len(pair) == 2:
			host = pair[0]
			port = int(pair[1])
		else:
			port = 3306
		try:
			conn = MySQLdb.connect(host, user, passwd, db, port=port)
		except MySQLError as xxx_todo_changeme:
			(errno, strerror) = xxx_todo_changeme.args
			raise DatabaseError('%s - %s' % (str(errno), strerror))
		return conn
			
	@classmethod
	def send(cls, pard):
		try:
			return super().send(pard)
		except MySQLError as xxx_todo_changeme1:
			(errno, strerror) = xxx_todo_changeme1.args
			raise DatabaseError('%s - %s' % (str(errno), strerror))
		
	@classmethod
	def send_dict(cls, pard):
		try:
			return super().send_dict(pard)
		except MySQLError as xxx_todo_changeme2:
			(errno, strerror) = xxx_todo_changeme2.args
			raise DatabaseError('%s - %s' % (str(errno), strerror))	
	
	@classmethod
	def get_db_tables_list(cls, pard):
		"""Return the tables lists in "pard['DB']" database"""
		pard['sql'] = "show tables"
		result = cls.send(pard)
		tables_list = []
		for rec in result:
			tables_list.append(rec[0])
		return tables_list
	
	@classmethod
	def get_db_tables_list_like(cls, pard):
		return []
	
	@classmethod
	def get_primary_key(cls, pard):
		return []
	
	@classmethod
	def show_columns(cls, pard, out='dict'):
		"""Return columns description from pard['table_name']
		The rows are formatted as a list of dictionaries by default.
		"""
		return []
		
	@classmethod
	def show_index(cls, pard):
		return [], []
	
	@classmethod
	def show_create_table(cls, pard):
		pard['sql'] = 'show create table %(table_name)s' % pard
		result = cls.send(pard)
		if result:
			return result[0][1]
		else:
			return ''

	@classmethod
	def limit_query(cls, sql_info: dict, query: str, limit: int) -> str:
		if 'limit' not in sql_info['words']:
			return f'{query} limit {limit}'

		return query
