import re
import sys
import os
import time
#import string
import random
import shelve
import pprint
import calendar
import traceback
import copy
import datetime
import hashlib
import binascii
import uuid
from decimal import Decimal


def format_messaggio(msg=[]):
	"""Formatta una lista di messaggi in una tabella html"""
	if isinstance(msg, str):
		msg = [msg]
	if msg:
		tab_html = []
		tab_html.append('<table width="500" cellpadding="0" cellspacing="2" border="0">')
		for elem in msg:
			tab_html.append('<tr><td class=tb3blu>%s</td></tr>' % elem)
		tab_html.append('</table>')
		#tab_html = string.join(tab_html, '\n')
		tab_html = '\n'.join(tab_html)
		return tab_html # + '<br>'
	else:
		return ''


def get_time():
	now = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime(time.time()))
	return now
		

def dump(pard):
	import pprint
	print('Content-type: text/html\n\n')
	print('<pre>')
	print('====================')
	#print pard['header']
	#print '===================='
	if type(pard) is type('string'):
		print(pard)
	else:
		pprint.pprint(pard)
	#s = ''
	#for k in pard.keys():
	#	s = s + '<b>' + k + '</b>: ' + repr(pard[k]) + '<br>\n'
	#print s
	print('====================')
	print('</pre>')


def make_pop(pop_name, lista, selected_value, jscript=''):
	html = '<select name="%s" id="%s" %s>' % (pop_name, pop_name, jscript)
	flag_selected = ''
	for item in lista:
		id = item[0]
		codice = item[1]
		selected = ''
		if item[0] == selected_value:
			selected = 'selected'
			flag_selected = 'FOUND'
		else:
			if flag_selected == '':
				try:
					if item[2] == 'def':
						selected = 'selected'
				except:
					pass
		html = html + '<option value="%s" %s>%s</option>' % (id, selected, codice)
	html = html + '</select>'
	return html


def dict_to_table(pard, dict_list, field_list=[], result_type=''):
	if field_list == []:
		field_list = list(dict_list[0].keys())
	l = []
	l.append('\t'.join(field_list))
	for d in dict_list:
		ll = []
		for key in field_list:
			if d[key] == None:
				d[key] = ''
			ll.append(str(d[key]))
		l.append('\t'.join(ll))
	
	if result_type == 'ftext':
		return table_to_text(pard, l)
	else:
		return '\n'.join(l)


def table_to_text(pard, ll):
	"""Data una lista di righe con i campi separati da tabulatori
	restitusce un testo formattato tipo query "mysql"
	"""
	column_lenght_dict = {}
	for line in ll:
		llc = line.split('\t')
		i = 0
		for col in llc:
			column_lenght_dict[i] = column_lenght_dict.get(i, 2)
			column_lenght_dict[i] = max(column_lenght_dict[i], len(col) + 2)
			i += 1
	sep = ['+']
	for i in range(len(ll[0].split('\t'))):
		sep.append('-'*column_lenght_dict[i])
		sep.append('+')
	sep = ''.join(sep)
	t = [sep]
	j = 0
	for line in ll:
		llc = line.split('\t')
		i = 0
		tl = ['|']
		for col in llc:
			tl.append(' %s |' % col.ljust(column_lenght_dict[i]-2))
			i += 1
		tl = ''.join(tl)
		t.append(tl)
		if j == 0 or j == (len(ll) - 1):
			t.append(sep)
		j += 1
	return '\n'.join(t)


def dict_to_insert(dict_list, field_list):
	l = ['insert into #replace_this_with_table_name#']
	l.append('(' + ', '.join(field_list) + ')')
	l.append('values')
	for d in dict_list:
		ll = []
		for key in field_list:
			s = d.get(key, '')
			if s == None:
				s = 'Null'
#			elif isinstance(s, str):
#				s = s.encode('utf8', 'replace')
#				s = "'%s'" % s.replace("'", "''")
			elif isinstance(s, str):
				s = "'%s'" % s.replace("'", "''")
			elif isinstance(s, int) or  isinstance(s, float):
				s = str(s)
			elif isinstance(s, Decimal):
				s = str(s)
			else:
				s = str(s)
				s = "'%s'" % s.replace("'", "''")
				
			ll.append(s)
		l.append('(' + ', '.join(ll) + '),')
	res = '\n'.join(l)[:-1] # Tolgo l'ultima virgola
	return res


def build_exception(type=None, value=None, tb=None, limit=None):
	if type is None:
		type, value, tb = sys.exc_info()
	buf = "traceback (most recent call last):"
	list = traceback.format_tb(tb, limit) + traceback.format_exception_only(type, value)
	buf = buf + "%s\n%s" % ("".join(list[:-1]), list[-1])
	del tb
	return buf


def get_last_exception():
	type, value, tb = sys.exc_info()
	r = traceback.format_exception_only(type, value)[-1]
	if r[-1] == '\n':
		r = r[:-1]
	return r


def escape_javascript(s):
	s = s.replace('\\', '\\\\').replace("'", "\\'").replace('\n', '\\n').replace('\r', '\\r').replace('"', '\\"')
	return s


def sbianca(rec={}):
	for key in list(rec.keys()):
		if rec[key] == '':
			rec[key] = '&nbsp;'
	return rec


def check_dir(dir_name):
    """Controlla se esiste la directory"""
    if os.path.exists(dir_name):
        if os.path.isdir(dir_name):
            pass
        else:
            os.remove(dir_name)
            os.mkdir(dir_name)
    else:
        os.mkdir(dir_name)


def sort_by(l, n, key=None):
    # case insensitive sort for string:
    # key = ci_sort = lambda (key, x): key.lower()
    nlist = list(map(lambda x, n=n: (x[n], x), l))
    nlist.sort(key=key)
    return [key_x[1] for key_x in nlist]


def lightness(s):
	if s.startswith('#'):
		s = s[1:]
	r,g,b = int(s[0:2],16), int(s[2:4],16), int(s[4:6],16)
	r1, g1, b1 = r/255, g/255, b/255
	l = (max(r1, g1, b1) + min(r1,g1,b1))/2
	return int(l*100)


## Animali ------------------------------------------------------------------
animali = ('agnello', 'airone', 'alce', 'alligatore', 'allodola', 'alpaca', 'anaconda', 'anatra', 'anatroccolo', 'anguilla', 'antilope', 'ape', 'aquila', 'aragosta', 'armadillo', 'asino', 'astice', 'babbuino', 'baco', 'balena', 'balenottera', 'barbagianni', 'bertuccia', 'biscia', 'bisonte', 'boa', 'bradipo', 'bruco', 'bue', 'bufalo', 'caimano', 'camaleonte', 'cammello', 'canarino', 'cane', 'canguro', 'capra', 'castoro', 'cavallo', 'cerbiatto', 'cervo', 'cicala', 'cicogna', 'cigno', 'cincilla', 'cinghiale', 'civetta', 'cobra', 'coccinella', 'coccodrillo', 'colibri', 'coniglio', 'corvo', 'criceto', 'daino', 'delfino', 'dentice', 'dingo', 'dinosauro', 'donnola', 'drago', 'dromedario', 'dugongo', 'echidna', 'elefante', 'elefante', 'marino', 'emu', 'ermellino', 'facocero', 'fagiano', 'faina', 'falco', 'falena', 'farfalla', 'fenicottero', 'foca', 'formica', 'formichiere', 'fringuello', 'fuco', 'furetto', 'gabbiano', 'gallina', 'gallo', 'gatto', 'gattopardo', 'gazzella', 'geco', 'ghepardo', 'ghiro', 'giaguaro', 'giraffa', 'girino', 'gnu', 'gorilla', 'granchio', 'grillo', 'gru', 'gufo', 'ibis', 'iena', 'iguana', 'impala', 'insetto', 'ippocampo', 'ippopotamo', 'istrice', 'lama', 'lemure', 'leone', 'leopardo', 'lepre', 'libellula', 'licaone', 'lince', 'lombrico', 'lontra', 'luccio', 'lucciola', 'lucertola', 'lumaca', 'lupo', 'macaco', 'maiale', 'mammut', 'mandrillo', 'mangusta', 'manta', 'mantide', 'marmotta', 'martora', 'medusa', 'merlo', 'micio', 'montone', 'mosca', 'moscerino', 'mucca', 'muflone', 'mulo', 'murena', 'nandu', 'narvalo', 'nasello', 'nibbio', 'nitticora', 'nottola', 'nutria', 'oca', 'opossum', 'orango', 'orangotango', 'orata', 'orca', 'orco', 'ornitorinco', 'orso', 'ostrica', 'otaria', 'paguro', 'panda', 'pantera', 'papera', 'pappagallo', 'passero', 'pavone', 'pecora', 'pesce', 'picchio', 'piccione', 'pinguino', 'pipistrello', 'pitone', 'polipo', 'pollo', 'polpo', 'porco', 'porcospino', 'procione', 'pulce', 'pulcino', 'puma', 'puzzola', 'raganella', 'ragno', 'ramarro', 'rana', 'ranocchio', 'ratto', 'razza', 'renna', 'riccio', 'rinoceronte', 'rondine', 'rospo', 'salamandra', 'salmone', 'sanguisuga', 'sardina', 'scimmia', 'scoiattolo', 'scorpione', 'scrofa', 'seppia', 'serpe', 'serpente', 'sogliola', 'somaro', 'sorcio', 'squalo', 'stambecco', 'struzzo', 'suino', 'tacchino', 'talpa', 'tapiro', 'tarantola', 'tartaruga', 'tasso', 'testuggine', 'tigre', 'tonno', 'topo', 'toro', 'tortora', 'tricheco', 'trota', 'tucano', 'uccello', 'upupa', 'usignolo', 'vacca', 'varano', 'verme', 'vespa', 'vipera', 'visone', 'vitello', 'volpe', 'vongola', 'zanzara', 'zebra', 'zebu', 'zecca', 'zibellino', 'zibetto')
colori = ('arancione', 'bianco', 'blu', 'giallo', 'grigio', 'marrone', 'nero', 'rosso', 'verde', 'viola', 'rosa', 'azzurro')
colorif = ('arancione', 'bianca', 'blu', 'gialla', 'grigia', 'marrone', 'nera', 'rossa', 'verde', 'viola', 'rosa', 'azzurra')
efemminile = ('alce', 'antilope', 'ape', 'istrice', 'lepre', 'lince', 'mantide', 'pulce', 'rondine', 'serpe', 'testuggine', 'tigre', 'volpe')

def inventa_nome():
	an = random.choice(animali)
	if an[-1] == 'a' or an in efemminile:
		col = random.choice(colorif)
	else:
		col = random.choice(colori)
	name = an + '_' + col
	return name


ascii_pat = re.compile('^[\w_@\-\.]+$', re.ASCII)
def ascii_string(s, prop='string', lang='en', min_len=0):
	if hasattr(prop, '_name'):
		name = prop._name
	else:
		name = str(prop)
	if not isinstance(s, str):
		raise TypeError('`%s` must be string or unicode, %s found'
			% (name, type(s).__name__))
	if len(s) < min_len:
		raise ValueError(msg('almeno_4', lang) % (name, min_len))
	if not ascii_pat.match(s):
		raise ValueError(msg('solo_lettere_e_numeri', lang) % name)
	return s


def to_int(value, property_name='', lang='en'):
	"""Validate integer property.

	Returns:
		A valid value.

	Raises:
		ValueError if value is not an integer or long instance.
	"""
	if value is None:
		return value
	try:
		value = int(value)
	except:
		raise ValueError(msg('intero', lang) % property_name)
	if not isinstance(value, int) or isinstance(value, bool):
		raise ValueError(msg('intero', lang) % property_name)
	if value < -0x8000000000000000 or value > 0x7fffffffffffffff:
		raise ValueError(msg('troppo_lungo', lang) % property_name)
	return value


def email(email):
    eml = re.compile('^[a-zA-Z0-9-_.]+@[a-zA-Z0-9-_.]+[.][a-zA-Z0-9-_.]+$')
    m = eml.match(email)
    if m:
        return m.group()
    else:
        raise ValueError('`%s` doesn\'t look like an email address.' % email)


def msg(msg_code, lang):
	if lang not in ('en', 'it'):
		lang = 'en'	
	dict  = {
		'almeno_4': {'en': '`%s` must be at least %s characters long.',
		            'it': '`%s` dovrebbe essere di almeno %s lettere.'},
		
		'solo_lettere_e_numeri': {'en': 'Please, for `%s` try to use only letters, numbers or `-_.@`',
		                         'it': 'Qualcosa non va in `%s`, prova ad usare solo lettere numeri o i caratteri `-_.@`'},
		
		'intero': {'en': '%s must be int or long',
		           'it': '%s non sembra essere un numero intero'},
		
		'troppo_lungo': {'en': 'Property %s must fit in 64 bits',
		                 'it': '%s e` veramente troppo grande come numero'},		
		}
	return dict[msg_code][lang]


def phash(password, salt=None, name='', min_len=0):
	password = ascii_string(password, name, min_len=min_len)
	if not salt:
		salt = os.urandom(32)
	key = hashlib.pbkdf2_hmac(
		'sha256', # The hash digest algorithm for HMAC
		password.encode('utf-8'), # Convert the password to bytes
		salt, # Provide the salt
		100000 # It is recommended to use at least 100,000 iterations of SHA-256 
		)
	hash_pw = salt + key
	return hash_pw


def digit_to_char(digit):
	if digit < 10:
		return str(digit)
	return chr(ord('a') + digit - 10)


def str_base(number,base):
	if number < 0:
		return '-' + str_base(-number, base)
	(d, m) = divmod(number, base)
	if d > 0:
		return str_base(d, base) + digit_to_char(m)
	return digit_to_char(m)


def get_alpha_token():
	x = binascii.b2a_hex(uuid.uuid4().bytes)
	n = int(x,16)
	return str_base(n,36)
