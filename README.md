# [MySQLweb]
MySQLweb is a web application that run queries, create and edit data, and view and export results, created and maintained by [Gianfranco Messori].

### Installation
#### Quick Start
* `git clone https://gieffe@bitbucket.org/gieffe/mysqlweb.git` or [download source from Bitbucket][1] 
* `cd mysqlweb`
* `pip3 install -r requirements.txt` (See "Install on MacOS" for detail)
* `cd bin`
* `gunicorn -c gunicorn.conf.py main:app`
* Point your browser to http://localhost:8088/
* `ctr-c` to stop

#### Authentication
The current version of MySQLweb is multi-user and requires authentication (unless you use LOCAL MODE).
On the first run, an administrator user "admin@mysqlweb.dev" or user specified in DEFAULT_USER parameter (See [Site Configuration](#SiteConfiguration)) will be created with a temporary password (see message on console on the first run).

#### LOCAL MODE
By default, mysqlweb runs in local mode, which means that the application can only be accessed from the machine where mysqlweb is hosted.
In local mode authentication is not required, but a DEFAULT_USER is still created and used to store preferences.  
See [Site Configuration](#SiteConfiguration)

#### Dependencies
MySQLweb runs on Python 3.x and should run on any Unix-like platform. It has dependencies with [mysqlclient], so you may need to install the Python 3 and MySQL development headers and libraries.	

#### Install on MacOS (Homebrew)

##### Install [Homebrew]

##### Install python3
```
brew install python3
```

##### Install mysql
```
brew install mysql
```

##### If you don't want to install MySQL server, you can use mysql-client instead:
```
brew install mysql-client
```

##### Upgrade pip3
```
python3 -m pip install --upgrade pip
```

##### Install MySQLweb requirements (including mysqlclient)
```
cd mysqlweb
pip3 install -r requirements.txt
```

In case of installation failure, and `mysql-client` brew package was previously installed,
you can try:

```
brew link --overwrite mysql-client
export PATH="/opt/homebrew/opt/mysql-client/bin:$PATH"
```


<a name="SiteConfiguration"></a>
#### Site Configuration ####
*mysqlweb* starts with default configuration.

The easy way to customize configuration is to create a `site_config.yaml` file
in the `bin` directory or in a dir of your choice setting `SITE_CONFIG_YAML` environment variable.


This is the default configuration:

```
HTTP_PORT: 8088
BASE_DIR: '..'
DATA_DIR: '../data'
MAX_ROW: 100
COOKIE_EXPIRES_DAYS: 10
DEFAULT_USER: 'admin@mysqlweb.dev'
LOCAL_MODE: True
```

> **Be careful!** MySQLweb was made to run in a *local protected network*. I advise developer against running the application over the internet.  

### Overview: What is MySQLweb?
MySQLweb is a web application that run queries, create and edit data, and view and export results. But there is more ...

#### DataBase Structure  
  Database Structure Browser, provides instant access to database schema and objects. 

#### Connection Manager  
  The Database Connections Panel enables developers to create, organize, and manage database connections 

#### History Panel  
  The History Panel provides complete session history of queries showing what queries were run and when. Developers can easily retrieve, review, re-run, append or modify previously executed SQL statement. 

#### Exec Procedures  
  Multiple queries can be executed simultaneously (in transaction - query have to be separated by semicolon) 

#### Save SQL  
  Developers can save and easily reuse  common Queries 

#### Dump SQL
  *Dump SQL* dumps results of a query to a file contains SQL insert statements.
  These stataments can be used for backup or transfer data to another SQL server.

#### Query Autocomplete  
  Press "esc" when you are typing queries and enjoy with Autocoplete-query feature! 

#### Query Wizard  
  Type wsel tablename [,tablename 2, ...] and run query: MySQLweb prepare for you select statement to query the specified tables.
  Type wins tablename and run query: MySQLweb prepares for you insert statement. 

#### Silk Icons Search  
  Type silk icon-pattern and run query: MySQLweb search Silk icon that matches the pattern specified. 

### Connect to MySQL database using an SSH Tunnel
  From the database connections panel you can access a new feature that allows you to configure tunnels to connect to MySQL over the ssh protocol.
  This function has an inline help explaining how to set the different parameters.


You can also continue to setup an SSH tunnel using command like this from Terminal:

	$ ssh -l youruser yourhost.com -p 22 -N -f -C -L 1234:yourdbserver.com:3306

### Keyboard Shortcuts
- Run current query:  `cmd+enter` or `ctrl+enter`
- Comment selection: `cmd+k` or `ctrl+k`
- Completion: `esc`

#### Docker Quick Start
+ TODO TODO TODO (Not yet updated to python 3)

### LICENSE
[MIT]

[Gianfranco Messori]: https://bitbucket.org/gieffe
[MySQLweb]: https://bitbucket.org/gieffe/mysqlweb
[mysqlclient]: https://pypi.org/project/mysqlclient/
[Twisted]: http://twistedmatrix.com/trac/wiki
[1]: https://bitbucket.org/gieffe/mysqlweb/downloads/
[MIT]: http://opensource.org/licenses/mit-license.php
[nginx]: http://nginx.org/
[Docker Readme]: docker/README.md
[Homebrew]: https://brew.sh
